# Build stage 1
FROM centos:centos8 as intermediate

ARG PYTHON_BIN
ARG PYTHON_LIB
ARG PYTHON_PATH

ENV PATH=${PYTHON_BIN}:${PATH}
ENV LD_LIBRARY_PATH=${PYTHON_LIB}:${LD_LIBRARY_PATH}

RUN yum install -y \
    cmake3 \
    diffutils \
    file \
    gcc-c++ \
    git \
    libffi-devel \
    make \
    openssl-devel \
    tree \
    which \
    zlib-devel \
    sqlite-devel \
    curl

WORKDIR /home/dev/centos-build

COPY . .

RUN cd external && mkdir build \
    && cd build \
    && cmake .. \
    && make \ 
    && make install

RUN pip2 install -r ./requirements/python/requirements.txt \
    && pip2 install --upgrade pip setuptools wheel \
    && pip3 install --upgrade pip setuptools wheel

# Build stage 2
FROM centos:centos8

ARG CMAKE_BIN
ARG PYTHON_BIN
ARG PYTHON_LIB
ARG PYTHON_PATH

COPY --from=intermediate /usr /usr

ENV PATH=${CMAKE_BIN}:${PYTHON_BIN}:${PATH}
ENV LD_LIBRARY_PATH=${PYTHON_LIB}:${LD_LIBRARY_PATH}

RUN alias python='export PYTHONPATH=/usr/local/python/lib/python2.7/site-packages:$PYTHONPATH;python2'
RUN alias python3='export PYTHONPATH=/usr/local/python/lib/python3.9/site-packages:$PYTHONPATH;python3'