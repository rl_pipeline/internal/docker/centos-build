# [centos-build](https://gitlab.com/rl_pipeline/internal/docker/centos-build.git)


### Overview
centos-build is a package to generate docker image using centos server as a base
with several build system tools installed.


### Disclaimer
This was built and tested locally under:
* docker-19.03.12
* Ubuntu 18.04.3 LTS


### Prerequisites
* docker
  

### How to build docker image
./build_dockerfile.sh [repository] [tag]
