#!/bin/sh
docker build \
    --build-arg CMAKE_BIN=/usr/local/cmake/bin \
    --build-arg PYTHON_BIN=/usr/local/python3/bin:/usr/local/python/bin \
    --build-arg PYTHON_LIB=/usr/local/python3/lib:/usr/local/python/lib \
    --tag $1:$2 \
    .